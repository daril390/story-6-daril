from django.shortcuts import render
from .forms import ResponseForm
from .models import response

# Create your views here.

def index(request):
    form = ResponseForm(request.POST or None)
    responses = response.objects.all()
    if form.is_valid():
        form.save()
        form = ResponseForm()
    context = {
        'form' : form,
        'respon' : responses,
    }

    return render(request, 'index.html', context)